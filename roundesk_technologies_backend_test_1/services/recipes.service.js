const recipes = require('../recipes.json')



async function getRecipesByStepId(Id, elapsedTime) {

    try {
        if (typeof Number(Id) === 'number') {
            const recipe = recipes.find(recipe => recipe.id == Id);
            if (!recipe)
                throw ("no recipes found ")
            else {
                const indexOfElapsedTime = recipe.timers.indexOf(Number(elapsedTime))
                const currentStep = recipe.steps[indexOfElapsedTime]
                return { recipe: recipe, currentStep: currentStep }
            }
        } else {
            throw ("INVALID ID ")

        }
    }
    catch (err) {
        next(err)

    }


}










module.exports = {
    getRecipesByStepId
}