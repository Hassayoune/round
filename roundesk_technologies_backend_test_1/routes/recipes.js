var recipes = require('../recipes.json');
var router = require('express').Router();
const recipesController = require("../controllers/recipes.controller") 



router.get('/step/:id',recipesController.getRecipesByStep)

module.exports = router;

