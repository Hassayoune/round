import React, { useEffect, useState } from 'react';
import './App.css';
import 'h8k-components';

import Articles from './components/Articles';

const title = "Sorting Articles";

const App = ({ articles }) => {
    const [dataArticles, setDataArticles] = useState(articles)
    const [sorted, setSorted]= useState(0)
    useEffect(() => {
    }, [dataArticles,sorted])
    
    const sortUpvoted = () => {
        const sortedData = dataArticles.sort((a, b) => a.upvotes < b.upvotes ? 1 : -1)
        setDataArticles(...sortedData)
    }
    const sortMostRecent = () => {
        const sortedData = dataArticles.sort((a, b) => a.date < b.date ? 1 : -1)
        setDataArticles(...sortedData)
    }

    return (
        <div className="App">
            <h8k-navbar header={title}></h8k-navbar>
            <div className="layout-row align-items-center justify-content-center my-20 navigation">
                <label className="form-hint mb-0 text-uppercase font-weight-light">Sort By</label>
                <button data-testid="most-upvoted-link" className="small" onClick={() => sortUpvoted()}>Most Upvoted</button>
                <button data-testid="most-recent-link" className="small" onClick={() => sortMostRecent()}>Most Recent</button>
            </div>
            <Articles articles={dataArticles} />
        </div>
    );
}

export default App;
