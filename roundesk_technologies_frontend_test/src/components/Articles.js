import React, { useEffect, useState } from 'react';

const Articles = ({articles}) => {

   
    const [dataArticles,setDataArticles] = useState(articles)


    return (
        

        <div className="card w-50 mx-auto">
            <table>
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Upvotes</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    {dataArticles.map(element => {

                        return (

                            <tr data-testid="article" key="article-index">
                                <td data-testid="article-title">{element.title}</td>
                                <td data-testid="article-upvotes">{element.upvotes}</td>
                                <td data-testid="article-date">{element.date}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    );

}

export default Articles;
